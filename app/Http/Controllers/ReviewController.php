<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review;

class ReviewController extends Controller
{
    public function review(Request $req)
    {
        #simpan review
        $review = new Review();
        $review->wisata_id = $req->wisata_id;
        $review->akun_id = $req->akun_id;
        $review->akun_nama = $req->akun_nama;
        $review->review = $req->review;
        $review->save();

        return redirect()->action('WisataController@showWisata',['id'=>$req->wisata_id]);
    }

}
