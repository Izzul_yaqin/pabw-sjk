<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Akun;
use App\Wisata;

class RegisterController extends Controller
{
    public function registerWisatawan(Request $req)
    {
        #simpan data akun
        $akun = new Akun();
        $akun->nama = $req->nama;
        $akun->email = $req->email;
        $akun->password = bcrypt($req->password);
        $akun->role = $req->role;
        $akun->save();

        return redirect('/');
    }

    public function wisata(Request $req)
    {
        #simpan data akun
        $akun = new Akun();
        $akun->nama = $req->nama;
        $akun->email = $req->email;
        $akun->password = bcrypt($req->password);
        $akun->role = $req->role;
        $akun->save();
        
        #simpan data wisata budaya
        $wisata = new Wisata();
        $wisata->akun_id = $akun->id;
        $wisata->kota_id = $req->kota;
        $wisata->nama_wisata = $req->nama_wisata;
        $wisata->alamat_wisata = $req->alamat_wisata;
        $wisata->deskripsi_wisata = $req->deskripsi_wisata;
        $wisata->htm_wisata = $req->htm;
        $wisata->status_wisata = $req->status;
        $wisata->save();
        
        #simpan jadwal
        $jumlah_hari = count($req->hari);
        for($i = 0; $i < $jumlah_hari; $i++){
            $jadwal = new HariWisata();
            $jadwal->hari_id = $req->hari[$i];
            $jadwal->wisata_id = $akun->id;
            $jadwal->save();
        }

        

        return redirect('/');
    }
}
