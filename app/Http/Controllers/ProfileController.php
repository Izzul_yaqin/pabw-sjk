<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use App\Akun;
use App\Wisata;

class ProfileController extends Controller
{
    #menampilkan profile wisatawan atau pengelola
    public function showProfile($id)
    {
        if(Session::get('role') == 'wisatawan'){

            $wisatawan = Akun::find($id);
            return view('profile/profile_wisatawan',['wisatawan'=>$wisatawan]);

        }elseif(Session::get('role') == 'pengelola'){

            $pengelola = Akun::find($id);
            return view('profile/profile_wisata', ['pengelola'=>$pengelola]);

        }
    }

    #menampilkan halaman edit wisatawan
    public function editWisatawan($id)
    {
        $wisatawan = Akun::find($id);
        return view('profile/profile_wisatawan_edit', ['wisatawan'=>$wisatawan]);
    }

    #simpan perubahan akun wisatawan
    public function editWisatawanStore($id, Request $req)
    {

        $wisatawan = Akun::find($id);

        if(Hash::check($req->password_confirm, $wisatawan->password)){
            $wisatawan->nama = $req->nama;
            $wisatawan->email = $req->email;
            if($req->password != null){
                $wisatawan->password = bcrypt($req->password);
            }else{
                $wisatawan->password = $wisatawan->password;
            }
            $wisatawan->save();
            Session::forget('nama');
            Session::put('nama', $req->nama);
            return redirect('/profile/'.$wisatawan->id);
        }else{
            return redirect('/profile/edit-wisatawan/'.$wisatawan->id);
        }
    }
}
